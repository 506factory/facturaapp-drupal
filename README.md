# FacturandoCR - Drupal 8

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/outerspacecoderscr/facturandocr.svg)](https://bitbucket.org/outerspacecoderscr/facturandocr/addon/pipelines/home)
[![Dashboard facturandocr](https://img.shields.io/badge/dashboard-facturandocr-yellow.svg)](https://dashboard.pantheon.io/sites/c3952e41-9a57-48a4-9952-f4d77662b5a9#dev/code)
[![Dev Site facturandocr](https://img.shields.io/badge/site-facturandocr-blue.svg)](http://dev-facturandocr.pantheonsite.io/)

This repository is a start state for a Composer-based Drupal workflow with Pantheon. It is meant to be copied by the the [Terminus Build Tools Plugin](https://github.com/pantheon-systems/terminus-build-tools-plugin) which will set up for you a brand new

* GitHub repo
* Free Pantheon sandbox site
* A CircleCI configuration to run tests and push from the source repo (GitHub) to Pantheon.

For more background information on this style of workflow, see the [Pantheon documentation](https://pantheon.io/docs/guides/github-pull-requests/).


## Installation

Clone the project from bitbucket repository.
Run composer install within the project folder.

### Prerequisites

Before running the `terminus build:project:create` command, make sure you have all of the prerequisites:

* [A Pantheon account](https://dashboard.pantheon.io/register)
* [Terminus, the Pantheon command line tool](https://pantheon.io/docs/terminus/install/)
* [The Terminus Build Tools Plugin](https://github.com/pantheon-systems/terminus-build-tools-plugin)
* An account with GitHub and an authentication token capable of creating new repos.
* PHP >= 7.2.14
* Git >= 2.20.1
* Composer >= 1.8.5


## Important files and directories

### `/web`

Pantheon will serve the site from the `/web` subdirectory due to the configuration in `pantheon.yml`, facilitating a Composer based workflow. Having your website in this subdirectory also allows for tests, scripts, and other files related to your project to be stored in your repo without polluting your web document root.

#### `/config`

One of the directories moved to the git root is `/config`. This directory holds Drupal's `.yml` configuration files. In more traditional repo structure these files would live at `/sites/default/config/`. Thanks to [this line in `settings.php`](https://github.com/pantheon-systems/example-drops-8-composer/blob/54c84275cafa66c86992e5232b5e1019954e98f3/web/sites/default/settings.php#L19), the config is moved entirely outside of the web root.

### `composer.json`

If you are just browsing this repository on GitHub, you may notice that the files of Drupal core itself are not included in this repo.  That is because Drupal core and contrib modules are installed via Composer and ignored in the `.gitignore` file. Specific contrib modules are added to the project via `composer.json` and `composer.lock` keeps track of the exact version of each modules (or other dependency). Modules, and themes are placed in the correct directories thanks to the `"installer-paths"` section of `composer.json`. `composer.json` also includes instructions for `drupal-scaffold` which takes care of placing some individual files in the correct places like `settings.pantheon.php`.


## Updating your site

When using this repository to manage your Drupal site, you will no longer use the Pantheon dashboard to update your Drupal version. Instead, you will manage your updates using Composer. Ensure your site is in Git mode, clone it locally, and then run composer commands from there.  Commit and push your files back up to Pantheon as usual.


## Pipelines

Bitbucket pipelines will be executed when there is a push made to staging, or master. They are the ones in charge of creating new environments on pantheon.


## Lando

To run the project locally, you will need to run lando start, and then lando pull within the project folder. Database and files don't need to be pulled, just the code.

## Deploy

When you are going to upload your code to pantheon, you must first push the code to staging (this will generate a new dev environment in pantheon), after everything have been reviewed and is working properly, do a pull request from staging to master and deploy on test environment and production from pantheon.
